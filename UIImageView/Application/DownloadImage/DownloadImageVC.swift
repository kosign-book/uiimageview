//
//  DownloadImageVC.swift
//  UIImageView
//
//  Created by Navy on 12/10/22.
//

import UIKit
import SkeletonView

class DownloadImageVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var downloadButton       : UIButton!
    @IBOutlet weak var generateLinkButton   : UIButton!
    @IBOutlet weak var imageLinkTextField   : UITextField!
    @IBOutlet weak var skeletonView         : UIView!
    
    // MARK: - Variable
    var imageLinkUrl = "https://images.pexels.com/photos/1525041/pexels-photo-1525041.jpeg?cs=srgb&dl=pexels-francesco-ungaro-1525041.jpg&fm=jpg"
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    // MARK: - Function
    func initView() {
        skeletonView.isHidden                   = true
        downloadButton.isUserInteractionEnabled = false
        downloadButton.backgroundColor          = .systemGray2
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        
        startAnimateSekeleton()
        
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.imageView.image = UIImage(data: data)
                self.stopAnimateSkeleton()
            }
        }
    }
    
    func startAnimateSekeleton() {
        skeletonView.isHidden = false
        skeletonView.isSkeletonable = true
        skeletonView.skeletonCornerRadius = 18
        skeletonView.showAnimatedGradientSkeleton()
    }
    
    func stopAnimateSkeleton() {
        skeletonView.isSkeletonable = false
        skeletonView.stopSkeletonAnimation()
        skeletonView.isHidden = true
    }
    
    // MARK: - IBAction
    @IBAction func backBarButtonItem(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func generateLinkButtonDidTap(_ sender: UIButton) {
        self.imageLinkTextField.text = imageLinkUrl
        self.downloadButton.isUserInteractionEnabled = true
        self.downloadButton.backgroundColor = .systemGreen
    }
    
    @IBAction func downloadButtonDidTap(_ sender: UIButton) {
        
        print("Begin of code")
        let url = URL(string: imageLinkTextField.text!)
        downloadImage(from: url!)
        // imageView.downloaded(from: "https://images.pexels.com/photos/355508/pexels-photo-355508.jpeg?cs=srgb&dl=pexels-pixabay-355508.jpg&fm=jpg")

        // End of code. The image will continue downloading in the background and it will be loaded when it ends
        
    }
    
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
