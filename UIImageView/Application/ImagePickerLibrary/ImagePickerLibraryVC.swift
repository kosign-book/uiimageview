//
//  ImagePickerLibraryVC.swift
//  UIImageView
//
//  Created by Navy on 11/10/22.
//

import UIKit

class ImagePickerLibraryVC: UIViewController {
    
    
    // MARK: - @IBOutlet
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    // MARK: - Variable
    var profileImage    : UIImage?
    var coverImage      : UIImage?
    var chooseImageType = ImageType.Cover
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - @IBAction
    @IBAction func chooseProfileButtonDidTap(_ sender: UIButton) {
        self.chooseImageType = .Profile
        self.selectImage()
    }
    
    @IBAction func chooseCoverButtonDidTap(_ sender: UIButton) {
        self.chooseImageType = .Cover
        self.selectImage()
    }
    
    
}


extension ImagePickerLibraryVC {
    
    // MARK: - Functions
    func openCamera() {
        DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let vc = self.VC(sbName: "CameraSB", identifier: "CameraVC") as! CameraVC
                vc.viewController = self
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func openGallery() {
        let imagePickerController = QBImagePickerController()
        imagePickerController.modalPresentationStyle     = .fullScreen
        imagePickerController.delegate                   = self
        imagePickerController.allowsMultipleSelection    = true
        imagePickerController.mediaType                  = .image
        imagePickerController.maximumNumberOfSelection   = 1
        imagePickerController.numberOfColumnsInPortrait  = 3
        
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func selectImage() {
        self.showImageSourceOption(camera: {
            self.openCamera()
        }) {
            self.openGallery()
        }
    }
}




// MARK: - Selected Image Delegate - From Camera
extension ImagePickerLibraryVC : SelectedImageDelegate {
    
    
    func didFinishSelectionImage(imageData: Data) {
        
        let imgData = UIImage(data: imageData)?.jpegData(compressionQuality: 0.8)
        
        switch chooseImageType {
        case .Profile:
            self.profileImage = UIImage(data: imgData!)
            self.profileImageView.image = self.profileImage
            
        default:
            self.coverImage = UIImage(data: imgData!)
            self.coverImageView.image = self.coverImage
        }
        
    }
}


// MARK: - UIImagePickerController
extension ImagePickerLibraryVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Selected Image Delegate - From Gallery
extension ImagePickerLibraryVC: QBImagePickerControllerDelegate {
    
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        dismiss(animated: true, completion: nil)
    }
    
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        
        if let phAsset = assets.first as? PHAsset {
            
            let manager = PHImageManager.default()
            let requestOptions              = PHImageRequestOptions()
            requestOptions.isSynchronous    = true
            requestOptions.deliveryMode     = .highQualityFormat
            requestOptions.resizeMode       = .exact
            requestOptions.isNetworkAccessAllowed = true
            
            self.dismiss(animated: true, completion: {
                manager.requestImageData(for: phAsset, options: requestOptions) { (assetData, assetString, orientation, infoDic) in
                    guard let data = assetData else { return }
                    
                    // setup picked image
                    DispatchQueue.main.async {
                        switch self.chooseImageType {
                        case .Cover:
                            self.coverImage = UIImage(data: data)
                            self.coverImageView.image = self.coverImage
                            
                        default:
                            self.profileImage = UIImage(data: data)
                            self.profileImageView.image = self.profileImage
                        }
                        
                    }
                }
            })
        }
    }
}


extension ImagePickerLibraryVC {
    
}
