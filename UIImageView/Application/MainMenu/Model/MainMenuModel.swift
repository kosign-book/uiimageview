//
//  MainMenuModel.swift
//  UIImageView
//
//  Created by Navy on 7/10/22.
//

import Foundation

struct MainMenuModel<T> {
    var value: T?
}

struct MainMenuInfo {
    var title: String
    var rowName: RowName
}
