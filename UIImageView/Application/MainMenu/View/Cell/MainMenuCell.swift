//
//  MainMenuCell.swift
//  UIImageView
//
//  Created by Navy on 7/10/22.
//

import UIKit

class MainMenuCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func configCell(title: String) {
        titleLabel.text = title
    }

}
