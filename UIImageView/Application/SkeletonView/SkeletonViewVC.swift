//
//  ViewController.swift
//  UIImageView
//
//  Created by Navy on 30/8/22.
//  Copyright © 2022 KOSIGN. All rights reserved.


import UIKit
import SkeletonView

class SkeletonViewVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var changeImageButton    : UIButton!
    @IBOutlet weak var skeletonView         : UIView!
    @IBOutlet weak var nameLabel            : UILabel!
    @IBOutlet weak var jobLabel             : UILabel!
    @IBOutlet weak var nameSkeletonView     : UIView!
    @IBOutlet weak var jobSkeletonView      : UIView!
    @IBOutlet weak var rightBarButtonItem   : UIBarButtonItem!
    @IBOutlet weak var profilePicture       : UIImageView!
    
    // MARK: - Property
    var imgData         : Data?
    
    
    // MARK: - LlfeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Functions
    func setupView() {
        changeImageButton.isHidden = true
        setupSkeletonViews()
    }
    
    func setupSkeletonViews(){
        profilePicture.image = UIImage(named: "")
        startAnimateSekeleton()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // show skeleton for 5 sec
            self.profilePicture.image = UIImage(named: "profile_1")
            self.stopAnimateSkeleton()
        }
    }
    
    func startAnimateSekeleton() {
        rightBarButtonItem.isEnabled = false
        skeletonView.isHidden = false
        skeletonView.isSkeletonable = true
        skeletonView.showAnimatedGradientSkeleton()
        
        nameSkeletonView.isSkeletonable = true
        nameSkeletonView.showAnimatedGradientSkeleton()
        
        jobSkeletonView.isSkeletonable = true
        jobSkeletonView.showAnimatedGradientSkeleton()
    }
    
    func stopAnimateSkeleton() {
        rightBarButtonItem.isEnabled = true
        
        skeletonView.isSkeletonable = false
        skeletonView.stopSkeletonAnimation()
        skeletonView.isHidden = true

        nameSkeletonView.stopSkeletonAnimation()
        nameSkeletonView.isHidden = true
        
        jobSkeletonView.stopSkeletonAnimation()
        jobSkeletonView.isHidden = true
    }
    
//    func openCamera() {
//        DispatchQueue.main.async {
//            if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                let vc = self.VC(sbName: "CameraSB", identifier: "CameraVC") as! CameraVC
//                vc.viewController = self
//                self.present(vc, animated: true, completion: nil)
//            }
//        }
//    }
    
//    func openGallery() {
//        let imagePickerController = QBImagePickerController()
//        imagePickerController.modalPresentationStyle     = .fullScreen
//        imagePickerController.delegate                   = self
//        imagePickerController.allowsMultipleSelection    = true
//        imagePickerController.mediaType                  = .image
//        imagePickerController.maximumNumberOfSelection   = 1
//        imagePickerController.numberOfColumnsInPortrait  = 3
//
//        self.present(imagePickerController, animated: true, completion: nil)
//    }

    
    // Edit
    @IBAction func rightBarButtonDidTap(_ sender: Any) {
//        changeImageButton.isHidden = false
    }
    
    @IBAction func selectImageDidTap(_ sender: Any) {
//        selectImage()
        print("OK")
    }
    
}

//extension SkeletonViewVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    func selectImage() {
//        self.showImageSourceOption(camera: {
//            self.openCamera()
//        }) {
//            self.openGallery()
//        }
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        self.dismiss(animated: true, completion: nil)
//    }
//}

// MARK: - Selected Image Delegate - From Camera
//extension SkeletonViewVC : SelectedImageDelegate {
//    func didFinishSelectionImage(imageData: Data) {
//        let imgData = UIImage(data: imageData)?.jpegData(compressionQuality: 0.8)
//
//        self.imgData = imgData
//
//        DispatchQueue.main.async {
//            self.profilePicture.image = UIImage(data: imgData!)
//        }
//    }
//}

// MARK: - Selected Image Delegate - From Gallery
//extension SkeletonViewVC: QBImagePickerControllerDelegate {
//
//    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
//
//        if let phAsset = assets.first as? PHAsset {
//
//            let manager = PHImageManager.default()
//            let requestOptions              = PHImageRequestOptions()
//            requestOptions.isSynchronous    = true
//            requestOptions.deliveryMode     = .highQualityFormat
//            requestOptions.resizeMode       = .exact
//            requestOptions.isNetworkAccessAllowed = true
//
//            self.dismiss(animated: true, completion: {
//                manager.requestImageData(for: phAsset, options: requestOptions) { (assetData, assetString, orientation, infoDic) in
//                    guard let data = assetData else { return }
//
//                    DispatchQueue.main.async {
//                        self.profilePicture.image = UIImage(data: data)
//                    }
//                }
//            })
//        }
//    }
//}
