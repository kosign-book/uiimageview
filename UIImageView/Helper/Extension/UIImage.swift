//
//  UIImage.swift
//  UIImageView
//
//  Created by Navy on 31/8/22.
//

import Foundation

extension UIImage {
    
    enum SendType : Int { // sending type
        case Full = 1 // full size image
        case HD = 2 // HD size image
        case SD = 3 // SD size image
    }
    
    enum ImageType : Int { // image type
        case Portrait = 1
        case Landscape = 2
        case Square = 3
    }
    
    struct CheckImage { // check image type
        
        /// check image if it is landscape, portrait, or square
        ///
        /// - Parameter image: any image you want to check
        /// - Returns: return image type
        static func checkImageType(image : UIImage) -> ImageType {
            let size : CGSize = image.size
            let width : CGFloat = size.width
            let height : CGFloat = size.height
            
            if width > height {
                return ImageType.Landscape
            } else if width < height {
                return ImageType.Portrait
            } else {
                return ImageType.Square
            }
        }
        
    }
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    private func getNewWidthAndHeight(fixW : Float,fixH : Float, originalH : Float,originalW: Float) -> CGSize {
        
        var newWidth : Float = 0.0
        var newHeight : Float = 0.0
        
        if originalW > fixW && originalH > fixH {
            newWidth = fixW
            newHeight = fixH
        } else if originalW > fixW && originalH < fixH {
            newWidth = fixW
            newHeight = originalH
        } else if originalW < fixW && originalH > fixH {
            newWidth = originalW
            newHeight = fixH
        } else if originalW < fixW && originalH < fixH {
            newWidth = originalW
            newHeight = originalH
        } else if originalW == fixW && originalH < fixH {
            newWidth = originalW
            newHeight = originalH
        }
            //fix crash on 28.02.2019, when image is too small
        else {
            newWidth = fixW
            newHeight = fixH
        }
        return CGSize(width: CGFloat(newWidth), height: CGFloat(newHeight))
    }
    
    class func resizingImage(image: UIImage,toType type : SendType) -> UIImage {
        
        let originalWidth : Float = Float(image.size.width)
        let originalHeight : Float = Float(image.size.height)
        
        let imageType : ImageType = CheckImage.checkImageType(image: image)
        
        var resizedImage : UIImage = UIImage()
        
        switch imageType {
        case .Landscape: // landscape image
            switch type {
            case .Full: // full size
                resizedImage = image
                break
            case .HD: // HD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 1440.0, fixH: 1080.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            default: // SD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 960.0, fixH: 720.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            }
            
        case .Portrait: // Portrait image
            switch type {
            case .Full: // full size
                resizedImage = image
                break
            case .HD: // HD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 1080.0, fixH: 1440.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            default: // SD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 720.0, fixH: 960.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            }
            
        default: // Square image
            switch type {
            case .Full: // full size
                resizedImage = image
                break
            case .HD: // HD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 1440.0, fixH: 1440.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            default: // SD size
                let widthAndHeight = UIImage().getNewWidthAndHeight(fixW: 960.0, fixH: 960.0, originalH: originalHeight, originalW: originalWidth)
                resizedImage = UIImage().resizeImage(image: image, targetSize: widthAndHeight)
                break
            }
        }
        
        return resizedImage
    }
}
